// Translatable strings for rl.hpp

#ifndef CPLUSPLUS_RLSTRINGS_H
#define CPLUSPLUS_RLSTRINGS_H

#ifdef __cplusplus
#define EXTERN extern
#else
#define EXTERN
#endif

EXTERN char* msg_earlystop;
EXTERN char* msg_rl;
EXTERN int sequence_is_running;

#endif // CPLUSPLUS_HELPER_FUNCTIONS_H
